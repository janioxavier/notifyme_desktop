package util;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.IOException;

import dao.EntityFactory;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import vision.NotifyMeApp;

public class SceneBuilder {

	private static Stage primaryStage;
	private static Stage secondStage;
	private static AnchorPane pane;
	private static Dimension dimension;

	public static void setPrimaryStage(Stage stage) {
		primaryStage = stage;
		secondStage = new Stage();
		primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
			public void handle(WindowEvent we) {
				EntityFactory.closeFactory();
			}
		});
	}

	/**
	 * mostra o primeiro estagio
	 */
	public static void showPrimaryStage() {
		primaryStage.show();
	}

	/**
	 * mostra o primeiro estagio e esconde o segundo estagio
	 */
	public static void showPrimaryAndHideSecondStage() {
		primaryStage.show();
		secondStage.hide();
	}

	/**
	 * mostra o segundo estagio
	 */
	public static void showSecondStage() {
		secondStage.show();
	}

	/**
	 * mostra o segundo estagio e esconde o primeiro estagio
	 */
	public static void showSecondAndHidePrimaryStage() {
		secondStage.show();
		primaryStage.hide();
	}

	/**
	 * 
	 * @param path
	 *            caminho do arquivo FMXL
	 * @return um componente carregado de um arquivo FXLM
	 */
	public static Object getComponent(String path) {

		setDimensions();
		
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(NotifyMeApp.class.getResource(path));

		try {
			return loader.load();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}
	
	public static void setDimensions(){
		dimension = Toolkit.getDefaultToolkit().getScreenSize();
		dimension.height -= 35;
		dimension.width += 5;
		
	}

	public static AnchorPane getPane() {
		return pane;
	}

	public static void addPaneComponent(Node novo) {
		pane.getChildren().add(novo);
	}

	public static void removePaneComponent(Node removed) {
		pane.getChildren().remove(removed);
	}

	public static void createHome() {

		pane = (AnchorPane) getComponent("HomePage.fxml");// ok kind of 
		primaryStage.setTitle("Notify-Me");
		primaryStage.setHeight(dimension.getHeight());
		primaryStage.setWidth(dimension.getWidth());
		primaryStage.setMaximized(true);
		primaryStage.setResizable(false);
		primaryStage.setScene(new Scene(pane));
	}

	public static void createCadastro() {

		pane = (AnchorPane) getComponent("Cadastro.fxml");// ok
		primaryStage.setTitle("Notify-Me - Cadastro");
		primaryStage.setResizable(false);
		primaryStage.setScene(new Scene(pane));
	}

	public static void createConfig() {

		pane = (AnchorPane) getComponent("Config.fxml");// ok
		primaryStage.setTitle("Notify-Me - Configurações");
		primaryStage.setHeight(dimension.getHeight());
		primaryStage.setWidth(dimension.getWidth());
		primaryStage.setResizable(false);
		primaryStage.setScene(new Scene(pane));
	}
	
	public static void createLogin() {

		pane = (AnchorPane) getComponent("Login.fxml");// ok
		primaryStage.setTitle("Notify-Me - Login");
		primaryStage.setResizable(false);
		primaryStage.setScene(new Scene(pane));
	}
	
	public static void createHelp() {

		pane = (AnchorPane) getComponent("Help.fxml");
		primaryStage.setTitle("Notify-Me - Help");
		primaryStage.setHeight(dimension.getHeight());
		primaryStage.setWidth(dimension.getWidth());
		primaryStage.setResizable(false);
		primaryStage.setScene(new Scene(pane));
	}

	public static void createBusca() {

		pane = (AnchorPane) getComponent("Busca.fxml");
		primaryStage.setTitle("Notify-Me - Busca");
		primaryStage.setHeight(dimension.getHeight());
		primaryStage.setWidth(dimension.getWidth());
		primaryStage.setResizable(false);
		primaryStage.setScene(new Scene(pane));
		
	}

	public static void createContasVencidas() {

		pane = (AnchorPane) getComponent("ContasVencidas.fxml");
		primaryStage.setTitle("Notify-Me - Contas Vencidas");
		primaryStage.setHeight(dimension.getHeight());
		primaryStage.setWidth(dimension.getWidth());
		primaryStage.setResizable(false);
		primaryStage.setScene(new Scene(pane));
		
	}

	public static void createContasArquivadas() {

		pane = (AnchorPane) getComponent("ContasArquivadas.fxml");
		primaryStage.setTitle("Notify-Me - Contas Arquivadas");
		primaryStage.setHeight(dimension.getHeight());
		primaryStage.setWidth(dimension.getWidth());
		primaryStage.setResizable(false);
		primaryStage.setScene(new Scene(pane));
		
	}

	public static void createContasAPagar() {

		pane = (AnchorPane) getComponent("ContasAPagar.fxml");
		primaryStage.setTitle("Notify-Me - Contas A Pagar");
		primaryStage.setHeight(dimension.getHeight());
		primaryStage.setWidth(dimension.getWidth());
		primaryStage.setResizable(false);
		primaryStage.setScene(new Scene(pane));
		
	}

	public static void createConta() {

		pane = (AnchorPane) getComponent("NovaConta.fxml");
		primaryStage.setTitle("Notify-Me - Nova Conta");
		primaryStage.setHeight(dimension.getHeight());
		primaryStage.setWidth(dimension.getWidth());
		primaryStage.setResizable(false);
		primaryStage.setScene(new Scene(pane));
		
	}


}
