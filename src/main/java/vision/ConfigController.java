package vision;

import java.util.ArrayList;
import java.util.List;

import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import util.SceneBuilder;

public class ConfigController {
	
	@FXML
	private Button perfilBt;
	
	@FXML
	private Button mudarLoginBt;
	
	@FXML
	private Button mudarSenhaBt;
	
	@FXML
	private Button deletarAccBt;
	
	@FXML
	private Button smsBt;
	
	@FXML
	private Button emailBt;
	
	@FXML
	private Button homeBt;
	
	@FXML
	private Button newConta;
	
	@FXML
	private Button buscaContabt;
	
	@FXML
	private MenuItem contasAPagarBt;
	
	@FXML
	private MenuItem contasVencidasBt;
	
	@FXML
	private MenuItem contasArquivadasBt;
	
	@FXML
	private Button qMBt;
	
	@FXML
	private MenuItem configBt;
	
	@FXML
	private MenuItem sairBt;
	
	
	private List<Node> nodes = null;
	
	
	public void resetSetup(){// limpa eventuais componentes da tela. caso n exista componentes cria a lista deles
		
		if(nodes == null){
			nodes = new ArrayList<Node>();
		}
		else{
			for(int i = 0; i < nodes.size(); i++)
				SceneBuilder.removePaneComponent((Node)nodes.get(i));

			nodes.clear();
		}	
	}
	
	
	@FXML
	public void handlerPerfil(){
		
		resetSetup();
	
		Label main , l1, l2;
		TextField nome, sobrenome;
		Button confirmar;
		
		main = new Label("Perfil do usu�rio");
		l1 = new Label("* Nome: ");
		l2 = new Label("* Sobrenome: ");
		nome = new TextField();
		sobrenome = new TextField();
		confirmar = new Button("Confirmar");
		
		
		main.setLayoutX(650);
		main.setLayoutY(100);
		main.setStyle("-fx-font-weight:Bold;");
		main.setVisible(true);
		nodes.add(main);
		SceneBuilder.addPaneComponent(main);
		
		l1.setLayoutX(565);
		l1.setLayoutY(150);
		l1.setVisible(true);
		nodes.add(l1);
		SceneBuilder.addPaneComponent(l1);
		
		l2.setLayoutX(535);
		l2.setLayoutY(200);
		l2.setVisible(true);
		nodes.add(l2);
		SceneBuilder.addPaneComponent(l2);
		
		nome.setLayoutX(615);
		nome.setLayoutY(147);
		nome.setVisible(true);
		nodes.add(nome);
		SceneBuilder.addPaneComponent(nome);
		
		sobrenome.setLayoutX(615);
		sobrenome.setLayoutY(197);
		sobrenome.setVisible(true);
		nodes.add(sobrenome);
		SceneBuilder.addPaneComponent(sobrenome);
		
		confirmar.setLayoutX(650);
		confirmar.setLayoutY(250);
		confirmar.setVisible(true);
		nodes.add(confirmar);
		SceneBuilder.addPaneComponent(confirmar);
			
		
	}
	
	@FXML
	public void handlerMudarLogin(){
		
		resetSetup();
		
		
		Label main , l1, l2;
		TextField login;
		PasswordField senha;
		Button confirmar;
		
		main = new Label("Mudar login");
		l1 = new Label("* Novo Login: ");
		l2 = new Label("* Senha: ");
		login = new TextField();
		senha = new PasswordField();
		confirmar = new Button("Atualizar");
		
		
		main.setLayoutX(650);
		main.setLayoutY(100);
		main.setStyle("-fx-font-weight:Bold;");
		main.setVisible(true);
		nodes.add(main);
		SceneBuilder.addPaneComponent(main);
		
		l1.setLayoutX(535);
		l1.setLayoutY(150);
		l1.setVisible(true);
		nodes.add(l1);
		SceneBuilder.addPaneComponent(l1);
		
		l2.setLayoutX(562);
		l2.setLayoutY(200);
		l2.setVisible(true);
		nodes.add(l2);
		SceneBuilder.addPaneComponent(l2);
		
		login.setLayoutX(615);
		login.setLayoutY(147);
		login.setVisible(true);
		nodes.add(login);
		SceneBuilder.addPaneComponent(login);
		
		senha.setLayoutX(615);
		senha.setLayoutY(197);
		senha.setVisible(true);
		nodes.add(senha);
		SceneBuilder.addPaneComponent(senha);
		
		confirmar.setLayoutX(650);
		confirmar.setLayoutY(250);
		confirmar.setVisible(true);
		nodes.add(confirmar);
		SceneBuilder.addPaneComponent(confirmar);
		
		
	
	}
	
	@FXML
	public void handlerMudarSenha(){
		
		resetSetup();
		
		Label main , l1, l2, l3;
		PasswordField senhaOld, senhaNew, senhaNewConfirm ;
		Button confirmar;
		
		main = new Label("Mudar Senha");
		l1 = new Label("* Senha antiga: ");
		l2 = new Label("* Senha nova: ");
		l3 = new Label("* Confirmar Senha: ");
		senhaOld = new PasswordField();
		senhaNew = new PasswordField();
		senhaNewConfirm = new PasswordField();
		confirmar = new Button("Atualizar");
		
		
		main.setLayoutX(650);
		main.setLayoutY(100);
		main.setStyle("-fx-font-weight:Bold;");
		main.setVisible(true);
		nodes.add(main);
		SceneBuilder.addPaneComponent(main);
		
		l1.setLayoutX(530);
		l1.setLayoutY(150);
		l1.setVisible(true);
		nodes.add(l1);
		SceneBuilder.addPaneComponent(l1);
		
		l2.setLayoutX(535);
		l2.setLayoutY(200);
		l2.setVisible(true);
		nodes.add(l2);
		SceneBuilder.addPaneComponent(l2);
		
		l3.setLayoutX(508);
		l3.setLayoutY(250);
		l3.setVisible(true);
		nodes.add(l3);
		SceneBuilder.addPaneComponent(l3);
		
		senhaOld.setLayoutX(615);
		senhaOld.setLayoutY(147);
		senhaOld.setVisible(true);
		nodes.add(senhaOld);
		SceneBuilder.addPaneComponent(senhaOld);
		
		senhaNew.setLayoutX(615);
		senhaNew.setLayoutY(197);
		senhaNew.setVisible(true);
		nodes.add(senhaNew);
		SceneBuilder.addPaneComponent(senhaNew);
		
		senhaNewConfirm.setLayoutX(615);
		senhaNewConfirm.setLayoutY(247);
		senhaNewConfirm.setVisible(true);
		nodes.add(senhaNewConfirm);
		SceneBuilder.addPaneComponent(senhaNewConfirm);
		
		confirmar.setLayoutX(650);
		confirmar.setLayoutY(300);
		confirmar.setVisible(true);
		nodes.add(confirmar);
		SceneBuilder.addPaneComponent(confirmar);
	
	}
	
	@FXML
	public void handlerDeletarAcc(){
		
		resetSetup();
		
		
		Label main , l1;
		PasswordField senha;
		Button confirmar;
		
		main = new Label("Deletar Conta");
		l1 = new Label("* Confirmar Senha: ");
		senha = new PasswordField();
		confirmar = new Button("Confirmar");
		
		
		main.setLayoutX(650);
		main.setLayoutY(100);
		main.setStyle("-fx-font-weight:Bold;");
		main.setVisible(true);
		nodes.add(main);
		SceneBuilder.addPaneComponent(main);
		
		l1.setLayoutX(509);
		l1.setLayoutY(150);
		l1.setVisible(true);
		nodes.add(l1);
		SceneBuilder.addPaneComponent(l1);
		
		senha.setLayoutX(615);
		senha.setLayoutY(147);
		senha.setVisible(true);
		nodes.add(senha);
		SceneBuilder.addPaneComponent(senha);
		
		confirmar.setLayoutX(650);
		confirmar.setLayoutY(200);
		confirmar.setVisible(true);
		nodes.add(confirmar);
		SceneBuilder.addPaneComponent(confirmar);
		
		
		
	}
	
	@FXML
	public void handlerSms(){
		
		resetSetup();
		
		Label main, main2;
		Label l1, l2, l3, l4, l5, l6;
		TextField hora, numero1, numero2;
		CheckBox notifyBefore, notifyDay, notifyAfter;
		Button confirmar;
		
		main = new Label("Notifica��o via SMS");
		main2 = new Label("N�mero do celular");
		l1 = new Label("* Hora padr�o da Notifica��o:");
		l2 = new Label("Notificar antes do vencimento:");
		l3 = new Label("Notificar no dia do vencimento:");
		l4 = new Label("Notificar ap�s o vencimento:");
		l5 = new Label("* N�mero 1:");
		l6 = new Label("N�mero 2:");
		
		hora = new TextField("07:00 h");
		notifyBefore = new CheckBox();
		notifyDay = new CheckBox();
		notifyAfter = new CheckBox();
		
		numero1 = new TextField();
		numero2 = new TextField();
		
		confirmar = new Button("Atualizar");
		
		
		
		main.setLayoutX(650);
		main.setLayoutY(100);
		main.setStyle("-fx-font-weight:Bold;");
		main.setVisible(true);
		nodes.add(main);
		SceneBuilder.addPaneComponent(main);
		
		
		main2.setLayoutX(650);
		main2.setLayoutY(350);
		main2.setStyle("-fx-font-weight:Bold;");
		main2.setVisible(true);
		nodes.add(main2);
		SceneBuilder.addPaneComponent(main2);
		
		
		l1.setLayoutX(451);
		l1.setLayoutY(150);
		l1.setVisible(true);
		nodes.add(l1);
		SceneBuilder.addPaneComponent(l1);
		
		
		l2.setLayoutX(446);
		l2.setLayoutY(200);
		l2.setVisible(true);
		nodes.add(l2);
		SceneBuilder.addPaneComponent(l2);
		
		
		l3.setLayoutX(442);
		l3.setLayoutY(250);
		l3.setVisible(true);
		nodes.add(l3);
		SceneBuilder.addPaneComponent(l3);
		
		
		l4.setLayoutX(455);
		l4.setLayoutY(300);
		l4.setVisible(true);
		nodes.add(l4);
		SceneBuilder.addPaneComponent(l4);
		
		
		l5.setLayoutX(542);
		l5.setLayoutY(400);
		l5.setVisible(true);
		nodes.add(l5);
		SceneBuilder.addPaneComponent(l5);
		
		
		l6.setLayoutX(548);
		l6.setLayoutY(450);
		l6.setVisible(true);
		nodes.add(l6);
		SceneBuilder.addPaneComponent(l6);
		
		
		hora.setLayoutX(615);
		hora.setLayoutY(147);
		hora.setVisible(true);
		nodes.add(hora);
		SceneBuilder.addPaneComponent(hora);
		
		
		notifyBefore.setLayoutX(615);
		notifyBefore.setLayoutY(197);
		notifyBefore.setVisible(true);
		nodes.add(notifyBefore);
		SceneBuilder.addPaneComponent(notifyBefore);
		
		
		notifyDay.setLayoutX(615);
		notifyDay.setLayoutY(247);
		notifyDay.setVisible(true);
		nodes.add(notifyDay);
		SceneBuilder.addPaneComponent(notifyDay);
		
		
		notifyAfter.setLayoutX(615);
		notifyAfter.setLayoutY(297);
		notifyAfter.setVisible(true);
		nodes.add(notifyAfter);
		SceneBuilder.addPaneComponent(notifyAfter);
		
		
		numero1.setLayoutX(615);
		numero1.setLayoutY(397);
		numero1.setVisible(true);
		nodes.add(numero1);
		SceneBuilder.addPaneComponent(numero1);
		
		
		numero2.setLayoutX(615);
		numero2.setLayoutY(447);
		numero2.setVisible(true);
		nodes.add(numero2);
		SceneBuilder.addPaneComponent(numero2);
			
		
		confirmar.setLayoutX(650);
		confirmar.setLayoutY(500);
		confirmar.setVisible(true);
		nodes.add(confirmar);
		SceneBuilder.addPaneComponent(confirmar);
		
		
	}
	
	@FXML
	public void handlerEmail(){
		
		resetSetup();
		
		
		Label main, main2;
		Label l1, l2, l3, l4, l5, l6, l7;
		TextField hora, email1, email2, email3;
		CheckBox notifyBefore, notifyDay, notifyAfter;
		Button confirmar;
		
		main = new Label("Notifica��o via E-Mail");
		main2 = new Label("E-Mails");
		l1 = new Label("* Hora padr�o da Notifica��o:");
		l2 = new Label("Notificar antes do vencimento:");
		l3 = new Label("Notificar no dia do vencimento:");
		l4 = new Label("Notificar ap�s o vencimento:");
		l5 = new Label("* E-Mail 1:");
		l6 = new Label("E-Mail 2:");
		l7 = new Label("E-Mail 3:");
		
		hora = new TextField("07:00 h");
		notifyBefore = new CheckBox();
		notifyDay = new CheckBox();
		notifyAfter = new CheckBox();
		
		email1 = new TextField();
		email2 = new TextField();
		email3 = new TextField();
		
		confirmar = new Button("Atualizar");
		
		
		
		main.setLayoutX(650);
		main.setLayoutY(100);
		main.setStyle("-fx-font-weight:Bold;");
		main.setVisible(true);
		nodes.add(main);
		SceneBuilder.addPaneComponent(main);
		
		
		main2.setLayoutX(650);
		main2.setLayoutY(350);
		main2.setStyle("-fx-font-weight:Bold;");
		main2.setVisible(true);
		nodes.add(main2);
		SceneBuilder.addPaneComponent(main2);
		
		
		l1.setLayoutX(451);
		l1.setLayoutY(150);
		l1.setVisible(true);
		nodes.add(l1);
		SceneBuilder.addPaneComponent(l1);
		
		
		l2.setLayoutX(446);
		l2.setLayoutY(200);
		l2.setVisible(true);
		nodes.add(l2);
		SceneBuilder.addPaneComponent(l2);
		
		
		l3.setLayoutX(442);
		l3.setLayoutY(250);
		l3.setVisible(true);
		nodes.add(l3);
		SceneBuilder.addPaneComponent(l3);
		
		
		l4.setLayoutX(455);
		l4.setLayoutY(300);
		l4.setVisible(true);
		nodes.add(l4);
		SceneBuilder.addPaneComponent(l4);
		
		
		l5.setLayoutX(542);
		l5.setLayoutY(400);
		l5.setVisible(true);
		nodes.add(l5);
		SceneBuilder.addPaneComponent(l5);
		
		
		l6.setLayoutX(548);
		l6.setLayoutY(450);
		l6.setVisible(true);
		nodes.add(l6);
		SceneBuilder.addPaneComponent(l6);
		
		
		l7.setLayoutX(548);
		l7.setLayoutY(500);
		l7.setVisible(true);
		nodes.add(l7);
		SceneBuilder.addPaneComponent(l7);
		
		
		hora.setLayoutX(615);
		hora.setLayoutY(147);
		hora.setVisible(true);
		nodes.add(hora);
		SceneBuilder.addPaneComponent(hora);
		
		
		notifyBefore.setLayoutX(615);
		notifyBefore.setLayoutY(197);
		notifyBefore.setVisible(true);
		nodes.add(notifyBefore);
		SceneBuilder.addPaneComponent(notifyBefore);
		
		
		notifyDay.setLayoutX(615);
		notifyDay.setLayoutY(247);
		notifyDay.setVisible(true);
		nodes.add(notifyDay);
		SceneBuilder.addPaneComponent(notifyDay);
		
		
		notifyAfter.setLayoutX(615);
		notifyAfter.setLayoutY(297);
		notifyAfter.setVisible(true);
		nodes.add(notifyAfter);
		SceneBuilder.addPaneComponent(notifyAfter);
		
		
		email1.setLayoutX(615);
		email1.setLayoutY(397);
		email1.setVisible(true);
		nodes.add(email1);
		SceneBuilder.addPaneComponent(email1);
		
		
		email2.setLayoutX(615);
		email2.setLayoutY(447);
		email2.setVisible(true);
		nodes.add(email2);
		SceneBuilder.addPaneComponent(email2);
		
		
		email3.setLayoutX(617);
		email3.setLayoutY(497);
		email3.setVisible(true);
		nodes.add(email3);
		SceneBuilder.addPaneComponent(email3);
			
		
		confirmar.setLayoutX(650);
		confirmar.setLayoutY(550);
		confirmar.setVisible(true);
		nodes.add(confirmar);
		SceneBuilder.addPaneComponent(confirmar);

	}
	
	
	@FXML
	public void handlerHomeBt(){
		SceneBuilder.createHome();
	}
	
	
	@FXML
	public void handlerCreateContaBt(){
		SceneBuilder.createConta();
	}
	
	
	@FXML
	public void handlerBuscaBt(){
		SceneBuilder.createBusca();
	}
	
	
	@FXML
	public void handlerContasAPagarBt(){
		SceneBuilder.createContasAPagar();
	}
	
	
	@FXML
	public void handlerContasVencidasBt(){
		SceneBuilder.createContasVencidas();
	}
	
	@FXML
	public void handlerContasArquivadasBt(){
		SceneBuilder.createContasArquivadas();
	}
	
	@FXML
	public void handlerQMBt(){
		SceneBuilder.createHelp();
	}
	
	@FXML
	public void handlerConfigBt(){
		SceneBuilder.createConfig();
	}
	
	@FXML
	public void handlerSairBt(){
		SceneBuilder.createLogin();
	}
	

}
