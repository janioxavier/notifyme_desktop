package vision;

import javafx.scene.Node;
import javafx.scene.layout.AnchorPane;
import util.SceneBuilder;

public class HandlerConfigOptions {
	
	private static AnchorPane parent;
	
	public HandlerConfigOptions(){
		parent = SceneBuilder.getPane();
	}
	
	public static void add(Node novo){
		parent.getChildren().add(novo);
	}

}
