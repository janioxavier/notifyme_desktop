package vision;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.TextAlignment;

public class ContaUiModel {
		
	public double x, y;
	
	private int id;
	
	private Label nome, data, valor;
	
	private Button visualize, edit, remove;
	
	private AnchorPane pane;
	
	
	public ContaUiModel(String nomenovo, String datanovo, String valornovo, int idnovo){
		id = idnovo;
		
		nome = new Label(nomenovo);
		data = new Label(datanovo);
		valor = new Label(valornovo);
		
		
		visualize = new Button("V");
		edit = new Button("E");
		remove = new Button("X");
		
		pane = new AnchorPane();
			
		nome.setStyle("-fx-font-weight:bold;-fx-background-color:whitesmoke;-fx-border-color:lightgrey");
		nome.setTextAlignment(TextAlignment.CENTER);// simplismente n�o funciona pra nada isso
		
		nome.setPrefWidth(150);
		
		data.setStyle("-fx-font-weight:bold;");
		valor.setStyle("-fx-font-weight:bold;");
		pane.setStyle("-fx-background-color:white;");
		pane.setStyle("-fx-border-color:lightgrey;");
		
		
		nome.setLayoutX(0);
		nome.setLayoutY(0);
		
		visualize.setLayoutX(30);
		visualize.setLayoutY(20);
		
		edit.setLayoutX(65);
		edit.setLayoutY(20);
		
		remove.setLayoutX(100);
		remove.setLayoutY(20);
		
		data.setLayoutX(10);
		data.setLayoutY(53);

		valor.setLayoutX(10);
		valor.setLayoutY(70);
		
		pane.setPrefSize(150 , 100 );
		
		
		pane.getChildren().add(nome);
		pane.getChildren().add(visualize);
		pane.getChildren().add(edit);
		pane.getChildren().add(remove);
		pane.getChildren().add(data);
		pane.getChildren().add(valor);
		
		
		switch(id){
		case 1:
			x = 30;
			y = 90;
			break;
		case 2:
			x = 336.5;
			y = 90;
			break;
		case 3:
			x = 673;
			y = 90;
			break;
		case 4:
			x = 1009.5;
			y = 90;
			break;
		case 5:
			x = 30;
			y = 290;
			break;
		case 6:
			x = 336.5;
			y = 290;
			break;
		case 7:
			x = 673;
			y = 290;
			break;
		case 8:
			x = 1009.5;
			y = 290;
			break;
		case 9:
			x = 30;
			y = 490;
			break;
		case 10:
			x = 336.5;
			y = 490;
			break;
		case 11:
			x = 673;
			y = 490;
			break;
		case 12:
			x = 1009.5;
			y = 490;
			break;
		}
			
	}
	
	public double getX(){
		return x;
	}
	
	public double getY(){
		return y;
	}
	
	public AnchorPane getPane(){
		return pane;
	}
	

}
