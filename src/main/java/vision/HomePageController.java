package vision;

import java.util.ArrayList;
import java.util.List;

import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.layout.AnchorPane;
import util.SceneBuilder;

public class HomePageController {

	private List<Node> nodes = null;

	public void resetSetup() {// limpa eventuais componentes da tela. caso n
								// exista componentes cria a lista deles

		if (nodes == null) {
			nodes = new ArrayList<Node>();
		} else {
			for (int i = 0; i < nodes.size(); i++)
				SceneBuilder.removePaneComponent((Node) nodes.get(i));

			nodes.clear();
		}
	}

	@FXML
	public void handlerHomeBt() {
		SceneBuilder.createHome();
	}

	@FXML
	public void handlerCreateContaBt() {
		SceneBuilder.createConta();
	}

	@FXML
	public void handlerBuscaBt() {
		SceneBuilder.createBusca();
	}

	@FXML
	public void handlerContasAPagarBt() {
		SceneBuilder.createContasAPagar();
	}

	@FXML
	public void handlerContasVencidasBt() {
		SceneBuilder.createContasVencidas();
	}

	@FXML
	public void handlerContasArquivadasBt() {
		SceneBuilder.createContasArquivadas();
	}

	@FXML
	public void handlerQMBt() {
		SceneBuilder.createHelp();
	}

	@FXML
	public void handlerConfigBt() {
		SceneBuilder.createConfig();
	}

	@FXML
	public void handlerSairBt() {
		SceneBuilder.createLogin();
	}

	@FXML
	public void handlerLoad() {

		resetSetup();

		for (int i = 1; i < 13; i++) {
			AnchorPane teste = new AnchorPane();
			ContaUiModel model = new ContaUiModel("teste " + (i - 1), "11/11/2016", "" + (i - 1) , i);
			teste = model.getPane();

			teste.setLayoutX(model.x);
			teste.setLayoutY(model.y);

			nodes.add(teste);
			SceneBuilder.addPaneComponent(teste);
		}

	}

}
