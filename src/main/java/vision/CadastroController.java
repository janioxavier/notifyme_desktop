package vision;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import util.SceneBuilder;

public class CadastroController {
	
	
	@FXML
	private TextField usuario;
	
	@FXML
	private PasswordField senha;
	
	@FXML
	private TextField nome;
	
	@FXML
	private TextField sobrenome;
	
	@FXML
	private TextField email;
	
	@FXML
	private TextField telefone;
	
	@FXML
	private Button cadastrar;
	
	@FXML
	private Button voltar;
	
	
	@FXML
	public void handlerVoltar(){
		SceneBuilder.createLogin();
	}
	
	
	@FXML
	public void handlerCadastrar(){
		
		
		// persistencias no bd
		SceneBuilder.createHome();
	}
	
	

}
