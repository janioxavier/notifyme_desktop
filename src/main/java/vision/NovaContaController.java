package vision;

import java.io.File;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import util.SceneBuilder;

public class NovaContaController {

	@FXML
	private TextField nome_conta;
	
	@FXML
	private TextArea descricao;
	
	@FXML
	private DatePicker data;
	
	@FXML
	private TextField valor;
	
	@FXML
	private TextField imagemUrl;
	
	@FXML
	private Button imagem;
	
	@FXML
	private Button confirmar;
	
	File file;
	
	
	@FXML
	public void handlerImagem(){
		file = new FileChooser().showOpenDialog(null);
		imagemUrl.setText(file.getName());
		
	}
	
	@FXML
	public void handlerConfirmar(){
		
		// persistencias non banco de dados
		SceneBuilder.createHome();
		
	}
	
	@FXML
	public void handlerHomeBt(){
		SceneBuilder.createHome();
	}
	
	
	@FXML
	public void handlerCreateContaBt(){
		SceneBuilder.createConta();
	}
	
	
	@FXML
	public void handlerBuscaBt(){
		SceneBuilder.createBusca();
	}
	
	
	@FXML
	public void handlerContasAPagarBt(){
		SceneBuilder.createContasAPagar();
	}
	
	
	@FXML
	public void handlerContasVencidasBt(){
		SceneBuilder.createContasVencidas();
	}
	
	@FXML
	public void handlerContasArquivadasBt(){
		SceneBuilder.createContasArquivadas();
	}
	
	@FXML
	public void handlerQMBt(){
		SceneBuilder.createHelp();
	}
	
	@FXML
	public void handlerConfigBt(){
		SceneBuilder.createConfig();
	}
	
	@FXML
	public void handlerSairBt(){
		SceneBuilder.createLogin();
	}
	
}
