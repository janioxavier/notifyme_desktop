package vision;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import util.SceneBuilder;

public class LoginController {
	
	
	
	@FXML
	private TextField usuario;
	
	@FXML
	private PasswordField senha;
	
	@FXML
	private Button logar;
	
	@FXML
	private Button cadastrar;

	
	@FXML
	public void handlerLogar(){
		
		SceneBuilder.createHome();
	}
	
	@FXML
	public void handlerCadastrar(){
		
		SceneBuilder.createCadastro();
		
	}
	
}
