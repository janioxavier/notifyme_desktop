package dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class EntityFactory {
	private static EntityManagerFactory factory;

	public static EntityManager createEntityManager() {
		if (factory == null) {
			factory = Persistence.createEntityManagerFactory("matriflix");
		}
		return factory.createEntityManager();
	}
	
	public static void closeFactory() {
		if (factory != null)
			factory.close();
	}
}